# CarCar

Team:

* Ben Werth - service
* Gary San Angelo - sales

## Step-by-step Instructions to Run the Project
1. make your own fork of this repository
2. open the terminal in the directory of where you want to clone the fork
3. enter command:
    git clone (enter repository url here)
4. now its time to build and run your server! run these commands in your terminal:
    docker volume create beta-data
    docker-compose build
    docker-compose up
5. View the project at http://localhost:3000/



## Diagram of the project
![alt text](image.png)




## Design

## Service microservice

Explain your models and integration with the inventory
microservice, here.
The Technician model has:
    First name (charfield)
    Last name (charfield)
    Employee id (charfield)
You can have many technicans so the first and last names can all be the same but each id has to be unique to that specific person. one tech can have many appointments, or an appointment can only have one tech. this is why it is also included into our Appointment model as a ForeignKey


AutomobileVo has:
    Vin (charfield and unique)
    Sold (boolean)
each vehicle has a unique number for it known as the Vin. it also has a status if it is sold or not.  from the poller.py we pull from the inventory database and update our AutomobileVo with the same Vin and sold status so that we have in our database


Appointment has:
    date time (DateTimeField)
    reason (textfield because it can be a lot of writing for many reasons)
    status (charfield)
    Vin (charfield)
    customer (charfield)
    technician (ForeignKey because there can only be one tech assigned to the appointment so it must be chosen from the list.)
the vin in appointment should not be confused with the vin from the AutomobileVo. any car can be brought into the shop for service, but not every car has been sold from the shop. so later we use both vins to compare each other so that we may find out which ones has been sold at the shop to set a vip status. there can only be one tech assigned to one appointment.


## Explicitly Defined URLs and ports for each of the services
List technicians | GET | http://localhost:8080/api/technicians/
Create a technician | POST | http://localhost:8080/api/technicians/
Delete a technician | DELETE | http://localhost:8080/api/technicians/<int:pk>/
List service appointments | GET | http://localhost:8080/api/appointments/
Create service appointment | POST | http://localhost:8080/api/appointments/
Delete service appointment | DELETE | http://localhost:8080/api/appointments/<int:id>
Cancel/Finish service appointment | PUT | http://localhost:8080/api/appointments/<int:id>
Port 8080:8000

## CRUD Route Documentation for each of the services
list all techs, get request:
    http://localhost:8080/api/technicians/
    example of list all:
        {
	"technicians": [
		{
			"id": 1,
			"first_name": "james",
			"last_name": "becker",
			"employee_id": "1"
		},
		{
			"id": 3,
			"first_name": "gary",
			"last_name": "becker",
			"employee_id": ""
		},
    ]
        }



create a tech, post request:
    http://localhost:8080/api/technicians/
    example of json submit
    {
			"first_name": "gary",
			"last_name": "becker",
			"employee_id": "2"
}



delete a tech, delelte request:
    chose the id of the tech that you wish to delete example 2
    http://localhost:8080/api/technicians/2/
    example delete submit:
        {
	"deleted": true
}



list all appointments, get request:
    http://localhost:8080/api/appointments/
    example of list all:
        {
	"appointments": [
		{
			"id": 2,
			"date_time": "2024-02-13T18:00:00+00:00",
			"reason": "tires",
			"status": "canceled",
			"vin": "1234",
			"customer": "ben",
			"technician": {
				"id": 1,
				"first_name": "james",
				"last_name": "becker",
				"employee_id": "1"
			}
		},
		{
			"id": 3,
			"date_time": "2024-02-13T18:00:00+00:00",
			"reason": "tires",
			"status": "finished",
			"vin": "1234",
			"customer": "ben",
			"technician": {
				"id": 1,
				"first_name": "james",
				"last_name": "becker",
				"employee_id": "1"
			}

        }
    ]
        }


create an appointment, post request:
    http://localhost:8080/api/appointments/
    example of json submit:
    {
    "date_time": "2024-02-13T18:00:00+00:00",
    "reason": "tires",
    "status": "created",
    "vin": "1234",
    "customer": "ben",
    "technician": 1
}

    status here is created but in react when you submit the form it automatically makes the status created

change status of an appointment, put request:
    http://localhost:8080/api/appointments/2/
    the id the appointment you want to change goes into 2
    example of json submit
        {
    "action": "cancel"
}
    action should either be cancel or finish as per what client wanted


delete an apointment delete request:
    http://localhost:8080/api/appointments/1/
        the id the appointment you want to change goes into 1
    example of return json:
    {
	"deleted": true
}



all of this is settled and taken care of in the actual react pages assciated












## Sales microservice


 the backend consists of 4 models for the sales microservice: AutomobileVO, Customer, SalesPerson, and Sales. Sales gets data from the other 3 models by interacting with them via foreign keys.

The AutomobileVO is a value object that gets data about the automobiles in the inventory using a poller. The sales poller automotically polls the inventory microservice for data, so the sales microservice is constantly getting the updated data.

The reason for integration between these two microservices is that when recording a new sale, you'll need to choose which car is being sold and that information lives inside of the inventory microservice.

## example of classes/models/attributes and how its used in django
## class Person(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)

AutoMobileVO
    this model has 2 attributes associated with it the VIN and sold status. the sold status is a booleanfield that represents true or false and this will indicate if the automobile has been sold or not. the VIN will be used as a unique ID for each individual automobile

    As explained above, the poller will connect the project automobile model to our automobile value object to make sure the data is updated correctly.

Customer
    this model has 4 attributes that use the Charfield. This will be used to input data on a form and display data on a page for the customer's first name, last name, phone number, and address.

Salesperson
    this model has 3 attributes that use Charfield. This will be used to input data on a form and show data on a page for the Salesperson's first name, last name, and employee id.

Sales
    the sales model will use 3 foreign keys and 1 charfield. the charfield is used to input and show the price of the sale, and the foreign key will grab the data from the other models; customer, Salesperson and AutoMobileVO.

    a foreign key defines a many to one relationship using django. In this microservice, A sale can have many customers, salespersons, and automobileVOs whereas one single automobile would have not have multiple sales in this scenario.

## Accessing Endpoints to Send and View Data - Access through Insomnia:

### Customers:


| Action | Method | URL
| ----------- | ----------- | ----------- |
| List customers | GET | http://localhost:8090/api/customers/
| Create a customer | POST | http://localhost:8090/api/customers/
| Show a specific customer | GET | http://localhost:8090/api/customers/id/

To create a Customer (SEND THIS JSON BODY):
```
{
	"first_name":"jon",
	"last_name":"jon",
	"address": "123 strestreet",
	"phone_number": "123-567-1018"
}
```
Return Value of Creating a Customer:
```
{
	"id: "1",
	"name": "John Johns",
	"address": "1212 Ocean Street",
	"phone_number": 9804357878
}
```
Return value of Listing all Customers:
```
{
	"customers": [
		{
			"href": "/api/customers/1/",
			"id": 1,
			"first_name": "one",
			"last_name": "two",
			"address": "four",
			"phone_number": "three"
		},
		{
			"href": "/api/customers/2/",
			"id": 2,
			"first_name": "bon",
			"last_name": "jon",
			"address": "123 strestreet",
			"phone_number": "123-567-1018"
		},
		{
			"href": "/api/customers/3/",
			"id": 3,
			"first_name": "ron",
			"last_name": "jon",
			"address": "123 strestreet",
			"phone_number": "123-567-1018"
		}
	]
}
```
### Salespeople:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List salespeople | GET | http://localhost:8090/api/salespeople/
| Salesperson details | GET | http://localhost:8090/api/salesperson/id/
| Create a salesperson | POST | http://localhost:8090/api/salespeople/
| Delete a salesperson | DELETE | http://localhost:8090/api/salesperson/id/


To create a salesperson (SEND THIS JSON BODY):
```
{
	"first_name":"Nick",
	"last_name":"dorito",
	"employee_id":12389
}
```
Return Value of creating a salesperson:
```
{
	"salesperson": {
		"href": "/api/salespeople/4/",
		"id": 4,
		"first_name": "Nick",
		"last_name": "dorito",
		"employee_id": 12389
	}
}
```
List all salespeople Return Value:
```
{
	"salespersons": [
		{
			"href": "/api/salespeople/2/",
			"id": 2,
			"first_name": "stuff",
			"last_name": "stuff",
			"employee_id": "stuff"
		},
		{
			"href": "/api/salespeople/3/",
			"id": 3,
			"first_name": "Mick",
			"last_name": "dorito",
			"employee_id": "12389"
		},
		{
			"href": "/api/salespeople/4/",
			"id": 4,
			"first_name": "Nick",
			"last_name": "dorito",
			"employee_id": "12389"
		}
	]..
}
```
### Sales:
- the id value to show a salesperson's sales is the **"id" value tied to a salesperson.**

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List all salesrecords | GET | http://localhost:8090/api/sales/
| Create a new sale | POST | http://localhost:8090/api/sales/
| Show salesperson's salesrecords | GET | http://localhost:8090/api/sales/id
List all Salesrecords Return Value:
```
{
	"sales": [
		{
			"id": 27,
			"price": "$420,420.00",
			"salesperson": {
				"href": "/api/salespeople/2/",
				"id": 2,
				"first_name": "stuff",
				"last_name": "stuff",
				"employee_id": "stuff"
			},
			"customer": {
				"href": "/api/customers/1/",
				"id": 1,
				"first_name": "one",
				"last_name": "two",
				"address": "four",
				"phone_number": "three"
			},
			"automobile": {
				"id": 5,
				"vin": "8D4RN5D14A4195121",
				"sold": true
			}
		},
		{
			"id": 28,
			"price": "$1.00",
			"salesperson": {
				"href": "/api/salespeople/2/",
				"id": 2,
				"first_name": "stuff",
				"last_name": "stuff",
				"employee_id": "stuff"
			},
			"customer": {
				"href": "/api/customers/1/",
				"id": 1,
				"first_name": "one",
				"last_name": "two",
				"address": "four",
				"phone_number": "three"
			},
			"automobile": {
				"id": 8,
				"vin": "3D4RN5D14A4195121",
				"sold": true
			}
		},
		{
			"id": 29,
			"price": "$2.00",
			"salesperson": {
				"href": "/api/salespeople/3/",
				"id": 3,
				"first_name": "Mick",
				"last_name": "dorito",
				"employee_id": "12389"
			},
			"customer": {
				"href": "/api/customers/1/",
				"id": 1,
				"first_name": "one",
				"last_name": "two",
				"address": "four",
				"phone_number": "three"
			},
			"automobile": {
				"id": 10,
				"vin": "JD4RN5D14A195121",
				"sold": true
			}
		},
		{
			"id": 30,
			"price": "$3.00",
			"salesperson": {
				"href": "/api/salespeople/4/",
				"id": 4,
				"first_name": "Nick",
				"last_name": "dorito",
				"employee_id": "12389"
			},
			"customer": {
				"href": "/api/customers/1/",
				"id": 1,
				"first_name": "one",
				"last_name": "two",
				"address": "four",
				"phone_number": "three"
			},
			"automobile": {
				"id": 9,
				"vin": "5D4RN5D14A195121",
				"sold": true
			}
		}
	]
}
```
Create a New Sale (SEND THIS JSON BODY):
```
{
	"price": "$420,420",
	"automobile": "8D4RN5D14A4195121",
	"salesperson":2,
	"customer": 1
}
```
Return Value of Creating a New Sale:
```
{
	"sales": [
		{
			"id": 27,
			"price": "$420,420.00",
			"salesperson": {
				"href": "/api/salespeople/2/",
				"id": 2,
				"first_name": "stuff",
				"last_name": "stuff",
				"employee_id": "stuff"
			},
			"customer": {
				"href": "/api/customers/1/",
				"id": 1,
				"first_name": "one",
				"last_name": "two",
				"address": "four",
				"phone_number": "three"
			},
			"automobile": {
				"id": 5,
				"vin": "8D4RN5D14A4195121",
				"sold": true
			}
		},
```
Show a Salesperson's Salesrecord Return Value:
```
{
	"id": 1,
	"price": 111000,
	"vin": {
		"vin": "111"
	},
	"salesperson": {
		"id": 1,
		"name": "Liz",
		"employee_number": 1
	},
	"customer": {
		"id",
		"name": "Martha Stewart",
		"address": "1313 Baker Street",
		"phone_number": "980720890"
	}
}
```
