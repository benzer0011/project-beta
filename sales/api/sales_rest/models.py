from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True, null=True)
    sold = models.BooleanField(default=False)




class Salesperson(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200, null=True)

    def get_api_url(self):
        return reverse("api_sales_person_detail", kwargs={"pk": self.pk})

class Customer(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=200, null=True)

    def get_api_url(self):
        return reverse("api_customer_detail", kwargs={"pk": self.pk})

class Sale(models.Model):
    price = models.CharField(max_length=200)
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobile",
        on_delete=models.CASCADE
    )
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="salesperson",
        on_delete=models.CASCADE
    )
    customer = models.ForeignKey(
        Customer,
        related_name="customer",
        on_delete=models.CASCADE
    )

    def get_api_url(self):
        return reverse("sales_detail", kwargs={"pk": self.pk})
