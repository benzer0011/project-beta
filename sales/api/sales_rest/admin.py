from django.contrib import admin
from .models import AutomobileVO, Sale, Salesperson, Customer


admin.site.register(AutomobileVO)
admin.site.register(Sale)
admin.site.register(Salesperson)
admin.site.register(Customer)
