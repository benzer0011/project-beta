from django.urls import path
from .views import api_sales_person_list, api_sales_person_detail, api_customer_list ,api_customer_detail , api_sales_list, sale_details


urlpatterns = [
    path("salespeople/", api_sales_person_list, name="api_sales_person_list"),
    path("salespeople/<int:pk>/",api_sales_person_detail,name="api_sales_person_detail" ),
    path("customers/", api_customer_list, name="api_customer_list"),
    path("customers/<int:pk>/", api_customer_detail, name="api_customer_detail"),
    path("sales/", api_sales_list, name="sales_list"),
    path("sales/<int:id>", sale_details, name="sales_detail")
]
