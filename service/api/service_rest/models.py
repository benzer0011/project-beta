from django.db import models


class Technician(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    employee_id = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name} - {self.employee_id}"


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return f"VIN: {self.vin} - Sold: {self.sold}"


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.TextField()
    status = models.CharField(max_length=20)
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=255)
    technician = models.ForeignKey(Technician, on_delete=models.CASCADE)

    def __str__(self):
        return f"Appointment for {self.customer} on {self.date_time} with {self.technician.first_name} {self.technician.last_name}"
