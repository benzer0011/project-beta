import React, { useState, useEffect } from 'react';

function CreateApt() {
    const [techs, setTechs] = useState([]);
    const [formData, setFormData] = useState({
        date_time: '',
        reason: '',
        vin: '',
        customer: '',
        technician: ''
    });

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechs(data.technicians);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const url = 'http://localhost:8080/api/appointments/';

        const appointmentData = {
            ...formData,
            status: 'created'
        };

        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(appointmentData),
            headers: {
                'Content-Type': 'application/json'
            }
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setFormData({
                date_time: '',
                reason: '',
                vin: '',
                customer: '',
                technician: ''
            });
        }
    };

    return (
        <div>
            <h2>Create Appointment</h2>
            <form onSubmit={handleSubmit}>
                <div>
                    <label htmlFor="date_time">Date and Time:</label>
                    <input
                        type="datetime-local"
                        id="date_time"
                        name="date_time"
                        value={formData.date_time}
                        onChange={handleChange}
                    />
                </div>
                <div>
                    <label htmlFor="reason">Reason:</label>
                    <input
                        type="text"
                        id="reason"
                        name="reason"
                        value={formData.reason}
                        onChange={handleChange}
                    />
                </div>
                <div>
                    <label htmlFor="vin">VIN:</label>
                    <input
                        type="text"
                        id="vin"
                        name="vin"
                        value={formData.vin}
                        onChange={handleChange}
                    />
                </div>
                <div>
                    <label htmlFor="customer">Customer:</label>
                    <input
                        type="text"
                        id="customer"
                        name="customer"
                        value={formData.customer}
                        onChange={handleChange}
                    />
                </div>
                <div>
                    <label htmlFor="technician">Technician:</label>
                    <select
                        id="technician"
                        name="technician"
                        value={formData.technician}
                        onChange={handleChange}
                    >
                        <option value="">Select Technician</option>
                        {techs.map(tech => (
                            <option key={tech.id} value={tech.id}>{tech.first_name} {tech.last_name}</option>
                        ))}
                    </select>
                </div>
                <button type="submit">Create</button>
            </form>
        </div>
    );
}

export default CreateApt;
