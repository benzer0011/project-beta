import React, { useEffect, useState } from 'react';

function SaleForm() {
  const [automobiles, setAutomobiles] = useState([]);
  const [salespersons, setSalesPersons] = useState([]);
  const [customers, setCustomers] = useState([]);

  useEffect(() => {
    fetchSalesPersons();
    fetchCustomers();
    fetchAutomobiles();
  }, []);

  const [formData, setFormData] = useState({
    price: '',
    automobile: '',
    salesperson: '',
    customer: '',
  });

  const [successMessage, setSuccessMessage] = useState('');

  const fetchAutomobiles = async () => {
    const url = 'http://localhost:8100/api/automobiles/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    }
  };
  const fetchCustomers = async () => {
    const url = 'http://localhost:8090/api/customers/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  };
  const fetchSalesPersons = async () => {
    const url = 'http://localhost:8090/api/salespeople/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSalesPersons(data.salespersons);
    }
  };



  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const response = await fetch('http://localhost:8090/api/sales/', {
        method: 'POST',
        body: JSON.stringify(formData),
        headers: {
          'Content-Type': 'application/json',
        },
      });
      if (response.ok) {
        setSuccessMessage('Sale created successfully!');
        const responseData = await response.json();
        const saleId = responseData.sale.id;


        const putUrl = `http://localhost:8100/api/automobiles/${formData.automobile}/`;
        const putConfig = {
          method: 'PUT',
          body: JSON.stringify({ sold: true }),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const putResponse = await fetch(putUrl, putConfig);

        setFormData({
          price: '',
          automobile: '',
          salesperson: '',
          customer: '',
        });
      }

    } catch (error) {
      console.error(error);
    }
  };


  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;



    setFormData({
      ...formData,


      [inputName]: value,
    });
  }
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a New Sale</h1>
          {successMessage && <p style={{ color: 'red' }}>{successMessage}</p>}
          <form onSubmit={handleSubmit} id="create-hat-form">

            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.automobile} required name="automobile" id="automobile" className="form-select">
                <option value="">Choose an automobile</option>
                {automobiles
                  .filter(automobile => !automobile.sold)
                  .map(automobile => {
                    return (
                      <option key={automobile.id} value={automobile.vin}>{automobile.vin}</option>
                    )
                  })}
              </select>
            </div>


            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.salesperson} required name="salesperson" id="salesperson" className="form-select">
                <option value="">Choose a Sales Person</option>
                {salespersons.map(salesperson => {
                  return (
                    <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name} {salesperson.last_name}</option>
                  )
                })}
              </select>
            </div>

            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.customer} required name="customer" id="customer" className="form-select">
                <option value="">Choose a Customer</option>
                {customers.map(customer => {
                  return (
                    <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                  )
                })}
              </select>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.price} placeholder="Price" required type="text" name="price" id="price" className="form-control" />
              <label htmlFor="name">Price</label>
            </div>

            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )

};
export default SaleForm;
