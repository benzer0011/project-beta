import React, { useState, useEffect } from 'react';

function CreateAuto() {
    const [color, setColor] = useState('');
    const [year, setYear] = useState('');
    const [vin, setVin] = useState('');
    const [modelId, setModelId] = useState('');
    const [models, setModels] = useState([]);

    useEffect(() => {
        fetchModels();
    }, []);

    const fetchModels = async () => {
        const response = await fetch('http://localhost:8100/api/models/');
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const response = await fetch('http://localhost:8100/api/automobiles/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                color: color,
                year: year,
                vin: vin,
                model_id: modelId,
            }),
        });

        if (response.ok) {
            setColor('');
            setYear('');
            setVin('');
            setModelId('');
        }
    };

    return (
        <div>
            <h2>Create Automobile</h2>
            <form onSubmit={handleSubmit}>
                <div>
                    <label htmlFor="color">Color:</label>
                    <input
                        type="text"
                        id="color"
                        value={color}
                        onChange={(e) => setColor(e.target.value)}
                    />
                </div>
                <div>
                    <label htmlFor="year">Year:</label>
                    <input
                        type="number"
                        id="year"
                        value={year}
                        onChange={(e) => setYear(e.target.value)}
                    />
                </div>
                <div>
                    <label htmlFor="vin">VIN:</label>
                    <input
                        type="text"
                        id="vin"
                        value={vin}
                        onChange={(e) => setVin(e.target.value)}
                    />
                </div>
                <div>
                    <label htmlFor="model">Model:</label>
                    <select id="model" value={modelId} onChange={(e) => setModelId(e.target.value)}>
                        <option value="">Select Model</option>
                        {models.map(model => (
                            <option key={model.id} value={model.id}>{model.name}</option>
                        ))}
                    </select>
                </div>
                <button type="submit">Create</button>
            </form>
        </div>
    );
}

export default CreateAuto;
