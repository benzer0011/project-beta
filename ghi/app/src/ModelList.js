import React, { useState, useEffect } from 'react';

function ModelList() {
    const [models, setModels] = useState([]);

    useEffect(() => {
        fetchData();
    }, []);

    const fetchData = async () => {
        try {
            const response = await fetch('http://localhost:8100/api/models/');
            if (response.ok) {
                const data = await response.json();
                setModels(data.models);
            }
        } catch (error) {
            console.error('Error fetching models:', error);
        }
    };

    return (
        <div>
            <h2>List of Models</h2>
            <table className="table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => (
                        <tr key={model.id}>
                            <td>{model.name}</td>
                            <td>{model.manufacturer.name}</td>
                            <td>
                                <img
                                    src={model.picture_url}
                                    alt=""
                                    width="200px"
                                    height="200px"
                                />
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default ModelList;
