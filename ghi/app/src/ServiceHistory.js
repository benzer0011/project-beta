import React, { useState, useEffect } from 'react';

function ServiceHistory() {
    const [vin, setVin] = useState('');
    const [serviceHistory, setServiceHistory] = useState([]);
    const [filteredServiceHistory, setFilteredServiceHistory] = useState([]);

    useEffect(() => {
        fetchData();
    }, []);

    const fetchData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
            const data = await response.json();
            setServiceHistory(data.appointments);
            setFilteredServiceHistory(data.appointments);
        }
    };

    const handleSearch = (e) => {
        e.preventDefault();
        const searchTerm = vin.trim().toLowerCase();
        const filteredData = serviceHistory.filter(entry => entry.vin.toLowerCase().includes(searchTerm));
        setFilteredServiceHistory(filteredData);
    };


    return (
        <div>
            <h2>Service History</h2>
            <form onSubmit={handleSearch}>
                <input
                    type="text"
                    value={vin}
                    onChange={(e) => setVin(e.target.value)}
                    placeholder="Enter VIN number"
                />
                <button type="submit">Search</button>
            </form>
            <table className="table">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer</th>
                        <th>Date and Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Appointment Status</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredServiceHistory.map((entry, index) => (
                        <tr key={index}>
                            <td>{entry.vin}</td>
                            <td>{entry.customer}</td>
                            <td>{entry.date_time}</td>
                            <td>{entry.technician.first_name} {entry.technician.last_name}</td>
                            <td>{entry.reason}</td>
                            <td>{entry.status}</td>

                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default ServiceHistory;
