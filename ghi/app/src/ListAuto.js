import React, { useState, useEffect } from 'react';

function ListAuto() {
    const [autos, setAutos] = useState([]);

    useEffect(() => {
        fetchData();
    }, []);

    const fetchData = async () => {
            const response = await fetch('http://localhost:8100/api/automobiles/');
            if (response.ok) {
                const data = await response.json();
                setAutos(data.autos);
            }
    };

    return (
        <div>
            <h2>List of Automobiles</h2>
            <table className="table">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Model</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {autos.map((auto, index) => (
                        <tr key={index}>
                            <td>{auto.vin}</td>
                            <td>{auto.model.name}</td>
                            <td>{auto.color}</td>
                            <td>{auto.year}</td>
                            <td>{auto.sold ? 'Yes' : 'No'}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default ListAuto;
