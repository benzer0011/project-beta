import React, { useState, useEffect } from 'react';

function ManufacturerList() {
    const [manufacturers, setManufacturers] = useState([]);

    useEffect(() => {
        fetchData();
    }, []);

    const fetchData = async () => {
        try {
            const response = await fetch('http://localhost:8100/api/manufacturers/');
            if (response.ok) {
                const data = await response.json();
                setManufacturers(data.manufacturers);

            } else {
                console.error('Error fetching manufacturers:', response.statusText);
            }
        } catch (error) {
            console.error('Error fetching manufacturers:', error);
        }
    };

    return (
        <div>
            <h2>List of Manufacturers</h2>
            <table className="table">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map(manufacturer => (
                        <tr key={manufacturer.id}>
                            <td>{manufacturer.name}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default ManufacturerList;
