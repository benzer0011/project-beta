import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AddTech from './AddTech';
import CreateApt from './CreateApt';
import ListApt from './ListApt';
import ListTech from './ListTech';
import ServiceHistory from './ServiceHistory';
import CreateAuto from './CreateAuto';
import ListAuto from './ListAuto';
import AddSalesPerson from './AddSalesPerson';
import SalesPersonList from './SalesPersonList';
import AddCustomer from './AddCustomer';
import CustomerList from './CustomerList';
import CreateManufacturer from './CreateManufacturer';
import ManufacturerList from './ManufacturerList';
import ModelList from './ModelList';
import AddModel from './AddModel';
import AddSale from './AddSale';
import SaleList from './SaleList';
import SalesPersonHistory from './SalesPersonHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/add-technician" element={<AddTech />} />
          <Route path="/list-technicians" element={<ListTech />} />
          <Route path="/create-appointment" element={<CreateApt />} />
          <Route path="/list-appointments" element={<ListApt />} />
          <Route path="/service-history" element={<ServiceHistory />} />
          <Route path="/create-auto" element={<CreateAuto />} />
          <Route path="/list-auto" element={<ListAuto />} />
          <Route path="/SalesPeople" element={<SalesPersonList />} />
          <Route path="/SalesPeople/new" element={<AddSalesPerson />} />
          <Route path="/Customers" element={<CustomerList/>} />
          <Route path="/Customers/new" element={<AddCustomer/>} />
          <Route path="/manufacturers" element={<ManufacturerList/>} />
          <Route path="/manufacturers/new" element={<CreateManufacturer/>} />
          <Route path="/models" element={<ModelList/>} />
          <Route path="/models/new" element={<AddModel/>} />
          <Route path="/sales/new" element={<AddSale/>} />
          <Route path="/sales" element={<SaleList/>} />
          <Route path="SalesPersonHistory" element={<SalesPersonHistory/>} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
//
