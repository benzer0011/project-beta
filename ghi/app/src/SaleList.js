import React, { useState, useEffect } from 'react';
function SaleList() {
    const [sales, setSales] = useState([]);
    useEffect(() => {
        fetchData();
    }, []);
    const fetchData = async () => {
        try {
            const response = await fetch('http://localhost:8090/api/sales/');
            if (response.ok) {
                const data = await response.json();
                setSales(data.sales);
            }
        } catch (error) {
            console.error('Error fetching sales:', error);
        }
    };
    return (
        <div>
            <h2>List of Sales</h2>
            <table className="table">
                <thead>
                    <tr>
                        <th>Salesperson Employee ID</th>
                        <th>Salesperson Name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => (

                        <tr key={sale.id}>
                            <td>{sale.salesperson.employee_id}</td>
                            <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                            <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{sale.price}</td>
                        </tr>
                    ))}

                </tbody>
            </table>
        </div>
    );
}
export default SaleList;
