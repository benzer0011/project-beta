import React, { useState, useEffect } from 'react';
function SalesPersonList() {
    const [salesPpls, setSalesPpls] = useState([]);
    useEffect(() => {
        fetchData();
    }, []);
    const fetchData = async () => {
        try {
            const response = await fetch('http://localhost:8090/api/salespeople/');
            if (response.ok) {
                const data = await response.json();
                setSalesPpls(data.salespersons);
            } else {
                console.error('Failed to fetch sales person:', response.statusText);
            }
        } catch (error) {
            console.error('Error fetching sales person:', error);
        }
    };
    return (
        <div>
            <h2>List of Sales People</h2>
            <table className="table">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {salesPpls.map(salesPpl => (

                        <tr key={salesPpl.id}>
                            <td>{salesPpl.employee_id}</td>
                            <td>{salesPpl.first_name}</td>
                            <td>{salesPpl.last_name}</td>
                        </tr>
                    ))}

                </tbody>
            </table>
        </div>
    );
}
export default SalesPersonList;
