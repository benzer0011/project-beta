import React, { useState, useEffect } from 'react';

function ListApt() {
    const [appointments, setAppointments] = useState([]);
    const [automobiles, setAutomobiles] = useState([]);

    useEffect(() => {
        const fetchAppointments = async () => {
                const response = await fetch('http://localhost:8080/api/appointments/');
                if (response.ok) {
                    const data = await response.json();
                    setAppointments(data.appointments);
                }
        };

        const fetchAutomobiles = async () => {
                const response = await fetch('http://localhost:8100/api/automobiles/');
                if (response.ok) {
                    const data = await response.json();
                    setAutomobiles(data.autos);
                }
        };

        fetchAppointments();
        fetchAutomobiles();
    }, []);

    const handleStatusUpdate = async (appointmentId, newStatus) => {
            const response = await fetch(`http://localhost:8080/api/appointments/${appointmentId}/`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ action: newStatus }),
            });

            if (response.ok) {
                const updatedAppointments = appointments.map(appointment => {
                    if (appointment.id === appointmentId) {
                        appointment.status = newStatus;
                    }
                    return appointment;
                });
                setAppointments(updatedAppointments);
            }
    };

    const handleDeleteAppointment = async (appointmentId) => {
            const response = await fetch(`http://localhost:8080/api/appointments/${appointmentId}/`, {
                method: 'DELETE',
            });

            if (response.ok) {
                const updatedAppointments = appointments.filter(appointment => appointment.id !== appointmentId);
                setAppointments(updatedAppointments);
            }
    };

    const pendingAppointments = appointments.filter(appointment => appointment.status === 'created');

    return (
        <div>
            <h2>List of Pending Appointments</h2>
            <table className="table">
                <thead>
                    <tr>
                        <th>Date and Time</th>
                        <th>Reason</th>
                        <th>Status</th>
                        <th>VIN</th>
                        <th>VIP</th>
                        <th>Customer</th>
                        <th>Technician ID</th>
                        <th>Technician Name</th>
                        <th>Update Status</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {pendingAppointments.map(appointment => (
                        <tr key={appointment.id}>
                            <td>{appointment.date_time}</td>
                            <td>{appointment.reason}</td>
                            <td>{appointment.status}</td>
                            <td>{appointment.vin}</td>
                            <td>{automobiles.some(auto => auto.vin === appointment.vin) ? 'Yes' : 'No'}</td>
                            <td>{appointment.customer}</td>
                            <td>{appointment.technician.id}</td>
                            <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                            <td>
                                <button onClick={() => handleStatusUpdate(appointment.id, 'cancel')}>Cancel</button>
                                <button onClick={() => handleStatusUpdate(appointment.id, 'finish')}>Finish</button>
                            </td>
                            <td>
                                <button onClick={() => handleDeleteAppointment(appointment.id)}>Delete</button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default ListApt;
