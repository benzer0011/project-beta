import React, { useState, useEffect } from 'react';
function CustomerList() {
    const [customers, setCustomers] = useState([]);
    useEffect(() => {
        fetchData();
    }, []);
    const fetchData = async () => {
        try {
            const response = await fetch('http://localhost:8090/api/customers/');
            if (response.ok) {
                const data = await response.json();
                setCustomers(data.customers);
            } else {
                console.error('Failed to fetch customers:', response.statusText);
            }
        } catch (error) {
            console.error('Error fetching customers:', error);
        }
    };
    return (
        <div>
            <h2>List of Customers</h2>
            <table className="table">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Phone Number</th>
                        <th>Address</th>
                    </tr>
                </thead>
                <tbody>
                    {customers.map(customer => (

                        <tr key={customer.id}>
                            <td>{customer.first_name}</td>
                            <td>{customer.last_name}</td>
                            <td>{customer.phone_number}</td>
                            <td>{customer.address}</td>
                        </tr>
                    ))}

                </tbody>
            </table>
        </div>
    );
}
export default CustomerList;
