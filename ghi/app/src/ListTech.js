import React, { useState, useEffect } from 'react';

function ListTech() {
    const [technicians, setTechnicians] = useState([]);

    useEffect(() => {
        fetchData();
    }, []);

    const fetchData = async () => {
            const response = await fetch('http://localhost:8080/api/technicians/');
            if (response.ok) {
                const data = await response.json();
                setTechnicians(data.technicians);
            }
    };

    return (
        <div>
            <h2>List of Technicians</h2>
            <table className="table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Employee ID</th>
                    </tr>
                </thead>
                <tbody>
                    {technicians.map(technician => (
                        <tr key={technician.id}>
                            <td>{technician.first_name} {technician.last_name}</td>
                            <td>{technician.employee_id}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default ListTech;
