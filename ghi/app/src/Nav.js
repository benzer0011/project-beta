import React from 'react';
import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" to="/add-technician">Add Technician</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/create-appointment">Create Appointment</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/list-appointments">List Appointments</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/list-technicians">List Technicians</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/service-history">Service History</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/create-auto">Create Automobile</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/list-auto">All Automobiles</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/SalesPeople/new">Add a Sales Person</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/SalesPeople">Salespeople</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/Customers/new">Add a Customer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/Customers">Customers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/manufacturers/new">Add Manufacturer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/manufacturers">Manufacturers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/models/new">Add Model</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/models">Model List</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/sales/new">Add Sale</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/sales">Sales</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/SalesPersonHistory">Salesperson History</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
