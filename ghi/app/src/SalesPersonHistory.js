import React, { useState, useEffect } from 'react';

function SalePersonHistory() {
    const [salesPersons, setSalesPersons] = useState([]);
    const [selectedSalesPerson, setSelectedSalesPerson] = useState('');
    const [sales, setSales] = useState([]);

    useEffect(() => {
        fetchData();
    }, []);

    const handleSalesPersonChange = (event) => {
        const value = parseInt(event.target.value);
        setSelectedSalesPerson(value);
    }

    const fetchData = async () => {
        try {
            const salesResponse = await fetch('http://localhost:8090/api/sales/');
            const salesPersonsResponse = await fetch('http://localhost:8090/api/salespeople/');

            if (salesResponse.ok && salesPersonsResponse.ok) {
                const salesData = await salesResponse.json();
                const salesPersonsData = await salesPersonsResponse.json();
                setSales(salesData.sales);
                setSalesPersons(salesPersonsData.salespersons);

            }
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };


    const filteredSales = sales.filter(sale => sale.salesperson.id === selectedSalesPerson);

    return (
        <div>
            <h2>List of Sales</h2>
            <div className="mb-3">
                <select
                    onChange={handleSalesPersonChange}
                    required
                    type="salesperson"
                    id="salesperson"
                    className="form-select"
                    value={selectedSalesPerson}
                >
                    <option value="">Salesperson</option>
                    {salesPersons.map((salesPerson) => (
                        <option key={salesPerson.id} value={salesPerson.id}>
                            {salesPerson.first_name} {salesPerson.last_name}
                        </option>
                    ))}
                </select>
            </div>
            <table className="table">
                <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredSales.map(sale => (
                        <tr key={sale.id}>
                            <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                            <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{sale.price}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default SalePersonHistory;
